#!/usr/bin/python3
# -*- coding: UTF-8 -*-

# Colores aplicables a las salidas en terminal.

# Colores Basicos =============================================================
print("\033[4;31m Colores Basicos \033[0m")

print("\033[0;30;47m Negro \033[0m 0;30m")
print("\033[0;31;47m Rojo \033[0m 0;31m")
print("\033[0;32;47m Verde \033[0m 0;32m")
print("\033[0;33;47m Cafe \033[0m 0;33m")
print("\033[0;34;47m Azul \033[0m 0;34m")
print("\033[0;35;47m Magenta \033[0m 0;35m")
print("\033[0;36;47m Cyan \033[0m 0;36m")
print("\033[0;37;40m Gris \033[0m 0;37m")

# Colores Resaltados ==========================================================
print("\033[1;4;31m Colores Resaltados \033[0m")

print("\033[1;30;40m Gris Obscuro \033[0m 1;30m")
print("\033[1;31;40m Rojo Obscuro \033[0m 1;31m")
print("\033[1;32;40m Verde Obscuro \033[0m 1;32m")
print("\033[1;33;40m Amarillo \033[0m 1;33m")
print("\033[1;34;40m Azul Obscuro \033[0m 1;34m")
print("\033[1;35;40m Magenta Obscuro \033[0m 1;35m")
print("\033[1;36;40m Cyan Obscuro \033[0m 1;36m")
print("\033[1;37;40m Blanco\033[0m 1;37m")
