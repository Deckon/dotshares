#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os

# Informacion del sistema =====================================================
print("\033[1;33mInformación del sistema: \033[0m")
os.system("uname -r")
print("\n")

os.system("lsb_release -a")
print("\n")


# Actualizacion del sistema====================================================
opcion1 = input("\033[1;31m¿Actualizar sistema?(s/n): \033[0m")

if (opcion1 == "s" or opcion1 == "S"):
    print("\033[1;33mActualizando sistema: \033[0m")
    os.system("sudo apt update && sudo apt upgrade")
else:
    print("\033[1;33mEl sistema no se actualizara. \033[0m")
print("\n")


# Instalacion de paquetes desde repositorio ===================================
print("\033[1;33mInstalacion de paquetes desde repositorio: \033[0m")
print("\033[1;33m1) HTop \t2) Tree \t3) Bleachbit \t4) VLC \033[0m")
print("\033[1;33m5) Gimp \t6) Inkscape \t7) Arc-Theme \t8) UGet \033[0m")
print("\033[1;33m9) Youtube-dl \t10) Gparted \t11) Gnome-Tweak-Tool \033[0m")

opcion2 = input("\033[1;31m¿Instalacion Automatica o Manual?(a/m): \033[0m")

if (opcion2 == "a" or opcion2 == "A"):
    print("\033[1;33mInstalando paquetes: \033[0m")

    os.system("sudo apt install htop tree bleachbit vlc gimp inkscape \
            arc-theme uget youtube-dl gparted gnome-tweak-tool")
else:
    print("\033[1;33m¿Paquetes a instalar?: \033[0m")

    cadena = ""
    lista = [
        "htop", "tree", "bleachbit", "vlc", "gimp", "inkscape",
        "arc-theme", "uget", "youtube-dl", "gparted",
        "gnome-tweak-tool"
        ]

    seleccion = str(input("Paquetes: "))
    seleccion = seleccion.split()
    seleccion = list(map(int, seleccion))

    for i in seleccion:
        cadena = lista[i - 1] + " " + cadena

    os.system("sudo apt install " + cadena)
print("\n")


# Instalacion de paqueteria de desarrollo python ==============================
opcion3 = input("\033[1;31m¿Instalacion de la paqueteria de desarrollo \
                para Python?(s/n): \033[0m")

if(opcion3 == "s" or opcion3 == "S"):
    print("\033[1;33mInstalando paquetes de desarrollo python: \033[0m")

    os.system("sudo apt install pylama python-pylama python3-pylama \
            python-pip python3-pip virtualenv python-virtualenv \
            python3-virtualenv git xterm pycodestyle \
            python-pycodestyle python3-pycodestyle")
else:
    print("\033[1;33mNo se instalo el paquete de desarrollo para \
        python\033[0m")
print("\n")


# Instalacion de Steam ========================================================
opcion4 = input("\033[1;33m¿Instalar Steam?(s/n): \033[0m")

if(opcion4 == "s" or opcion4 == "S"):
    print("\033[1;33mInstalando Steam: \033[0m")
    os.system("sudo apt install steam")
else:
    print("\033[1;33mNo se instalo Steam. \033[0m")
print("\n")


# Instalacion de VirtualBox ===================================================
opcion5 = input("\033[1;33m¿Instalar VirtualBox?(s/n): \033[0m")

if(opcion5 == "s" or opcion5 == "S"):
    print("\033[1;33mInstalando VirtualBox: \033[0m")
    os.system("sudo apt install virtualbox")
else:
    print("\033[1;33mNo se instalo VirtualBox. \033[0m")
print("\n")


# Validación y creación de carpeta TEMPORAL ===================================
regreso = os.getcwd()

try:
    os.makedirs("TEMPORAL")
except OSError:
    print("El directorio temporal ya existe")
    pass

os.chdir("TEMPORAL")


# Instalacion de Paquetes externos ============================================
print("\033[1;33mInstalacion de paquetes externos: \033[0m")
print("\n")


# Instalacion de Google Chrome ================================================
opcion6 = input("\033[1;33m¿Instalar Google-Chrome?(s/n): \033[0m")

url_chrome = "https://dl.google.com/\
    linux/direct/google-chrome-stable_current_amd64.deb"

if(opcion6 == "s" or opcion6 == "S"):
    print("\033[1;33mInstalando Google-Chrome: \033[0m")
    os.system("wget " + url_chrome)
    os.system("sudo dpkg -i google-chrome-stable_current_amd64")
else:
    print("\033[1;33mNo se instalo Google-Chrome. \033[0m")
print("\n")


# Instalacion de Atom =========================================================
opcion7 = input("\033[1;33m¿Instalar Atom?(s/n): \033[0m")

url_atom = "https://atom.io/download/deb"

if(opcion7 == "s" or opcion7 == "S"):
    estado_git = os.popen("dpkg -s git | grep Status").read()
    estado_xterm = os.popen("dpkg -s xterm | grep Status").read()
    cadena = "Status: install ok installed" + "\n"

    if(estado_git != cadena):
        os.system("sudo apt install git")
    else:
        pass

    if(estado_xterm != cadena):
        os.system("sudo apt install xterm")
    else:
        pass

    print("\033[1;33mInstalando Atom: \033[0m")
    os.system("wget " + url_atom)
    os.system("sudo dpkg -i deb.deb")

    # Instalar complementos para Atom ========================================
    opcion8 = input("\033[1;33m¿Instalar complementos \
        para Atom?(s/n): \033[0m")

    if(opcion8 == "s" or opcion8 == "S"):
        print("\033[1;33mInstalando complementos para Atom: \033[0m")
        os.system("apm install atom-python-run autocomplete-python \
        file-icons highlight-line highlight-selected linter-python \
        minimap minimap-autohider updater-notify \
        atom-material-syntax atom-material-syntax-dark \
        atom-material-syntax-light atom-material-ui \
        linter-python linter linter-ui-default intentions busy-signal")
    else:
        pass
else:
    print("\033[1;33mNo se instalo Atom. \033[0m")
print("\n")


# Instalacion de Gitkraken ================================================
opcion9 = input("\033[1;33m¿Instalar Gitkraken?(s/n): \033[0m")

url_gitkraken = "https://release.gitkraken.com/linux/gitkraken-amd64.deb"

if(opcion9 == "s" or opcion9 == "S"):
    estado_git = os.popen("dpkg -s git | grep Status").read()
    cadena = "Status: install ok installed" + "\n"

    if(estado_git != cadena):
        os.system("sudo apt install git")
    else:
        pass

    print("\033[1;33mInstalando Gitkraken: \033[0m")
    os.system("wget " + url_gitkraken)
    os.system("sudo dpkg -i gitkraken-amd64.deb")
else:
    print("\033[1;33mNo se instalo Gitkraken. \033[0m")
print("\n")


# Instalacion de DBeaver ================================================
opcion10 = input("\033[1;33m¿Instalar DBeaver?(s/n): \033[0m")

url_dbeaver = "https://dbeaver.jkiss.org/files/dbeaver-ce_latest_amd64.deb"

if(opcion10 == "s" or opcion10 == "S"):
    print("\033[1;33mInstalando DBeaver: \033[0m")
    os.system("wget " + url_dbeaver)
    os.system("sudo dpkg -i dbeaver-ce_latest_amd64.deb")
else:
    print("\033[1;33mNo se instalo DBeaver. \033[0m")
print("\n")


# Instalacion de Spotify ================================================
opcion11 = input("\033[1;33m¿Instalar Spotify?(s/n): \033[0m")

if(opcion11 == "s" or opcion11 == "S"):
    os.system("sudo apt-key \
    adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys \
    BBEBDCB318AD50EC6865090613B00F1FD2C19886 \
    0DF731E45CE24F27EEEB1450EFDC8610341D9410")

    os.system("echo deb  \
    http://repository.spotify.com stable non-free | sudo tee \
    /etc/apt/sources.list.d/spotify.list")

    os.system("sudo apt update")

    os.system("sudo apt install spotify-client")
else:
    print("\033[1;33mNo se instalo Spotify. \033[0m")
print("\n")


# Eliminar carpeta TEMPORAL ===================================================
os.chdir(regreso)
os.system("rm -r TEMPORAL")


# Instalacion de Oh-My-Zsh ====================================================
opcion10 = input("\033[1;33m¿Instalar Oh-My-Zsh?(s/n): \033[0m")


if(opcion10 == "s" or opcion10 == "S"):
    print("\033[1;33mInstalando Oh-My-Zsh: \033[0m")

    os.system("sudo apt install zsh git-core")
    os.system("wget \
    https://github.com/robbyrussell/oh-my-zsh/raw/master/tools \
    /install.sh -O - | zsh")
    os.system("chsh -s `which zsh`")
else:
    print("\033[1;33mNo se instalo Oh-My-Zsh. \033[0m")
print("\n")

# Configuraciones del sistema =================================================
os.system('echo "\n" >> .zshrc')
os.system('echo alias update="sudo apt update && sudo apt upgrade" >> .zshrc')
os.system('echo alias kernel=" \n\
echo KERNEL EN USO: \n\
uname -r \n\
echo ------------------------------------------------------\n\
echo LISTADO DE KERNELS: \n\
dpkg --get-selections | grep linux-image \n\
echo ------------------------------------------------------\n\
echo ELIMINAR KERNEL ANTIGUO: \n\
echo sudo apt-get remove --purge paquete" >> .zshrc')
